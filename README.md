# Cleanup Gitlab Runners Cron

Repo to remove dead runners to avoid mesages like this:

```
ERROR: Registering runner... failed                 runner=AB1234567890LYgXDgyb status=POST https://gitlab.com/api/v4/runners: 400 Bad Request (runner_namespaces.base: Maximum number of ci registered group runners (50) exceeded)
PANIC: Failed to register the runner. 
```

using : https://github.com/containeroo/cleanup-gitlab-runner

